# Java (Quarkus) API monitoring example

## Running the application in dev mode

You can run your application in dev mode that enables live coding using:
```shell script
./mvnw compile quarkus:dev
```

## Monitoring

Run Prometheus and Grafana with the docker-compose file. 

```sh
docker compose up -d
```

In Grafana, import the following Quarkus micrometer dashboard : https://grafana.com/grafana/dashboards/14370

This dashboard contains HTTP Endpoints monitoring.

## Result

![result](result.png)
