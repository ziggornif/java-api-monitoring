package xyz.ziggornif;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/tweets")
@Produces("application/json")
@Consumes("application/json")
public class TweetResource {
    @Inject
    TweetService tweetService;

    @GET
    public List<Tweet> get() {
        return tweetService.fetch();
    }

    @POST
    @Transactional
    public Response create(@Valid TweetRequest request) {
        Tweet created = tweetService.create(request.message);
        return Response.status(Response.Status.CREATED).entity(created).build();
    }

}
