package xyz.ziggornif;

import io.quarkus.panache.common.Sort;

import javax.enterprise.context.ApplicationScoped;
import java.util.Date;
import java.util.List;

@ApplicationScoped
public class TweetService {
    public List<Tweet> fetch() {
        return Tweet.listAll(Sort.by("created_at"));
    }

    public Tweet create(String message) {
        Tweet tweet = new Tweet();
        tweet.message = message;
        tweet.createdAt = new Date();
        tweet.persist();
        return tweet;
    }

}
